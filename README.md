# Wizard - Game server

This is an online server of the card game
[_Wizard_](<https://en.wikipedia.org/wiki/Wizard_(card_game)>). It is currently under
development but already functional. The language is currently a mix of German and
English. An i18n system would be nice but we will see.

## Installation

Either use `poetry install --no-dev` or `pip install .`. Launch the game via `uvicorn wizard.entry:app`. Have Fun!

## License

The application is licensed AGPL-3.0-or-later. I did **not** invent or create the game,
see the linked wikipedia page for more details.
