import asyncio
from collections import defaultdict
from enum import Enum
from typing import Dict
from typing import List
from typing import Optional
from typing import TypedDict
from typing import Union

from starlette.applications import Starlette
from starlette.requests import Request
from starlette.routing import Mount
from starlette.routing import Route
from starlette.routing import WebSocketRoute
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
from starlette.templating import _TemplateResponse
from starlette.websockets import WebSocket

from wizard.helper import bids_and_scores_table
from wizard.helper import card_to_json
from wizard.lib import pass_bid
from wizard.lib import pass_trump_wish
from wizard.lib import play_card
from wizard.lib import start_game
from wizard.lib import start_new_trick
from wizard.lib import update_scores
from wizard.models import Card
from wizard.models import Game
from wizard.models import GameName
from wizard.models import GameState
from wizard.models import Player
from wizard.models import Rank
from wizard.models import SpecialRules
from wizard.models import Suit
from wizard.models import special_rules_descriptions

templates = Jinja2Templates(directory="templates")

games: Dict[GameName, Game] = dict()
player_to_socket: Dict[GameName, Dict[Player, WebSocket]] = defaultdict(lambda: dict())
number_of_players: Dict[GameName, int] = dict()
special_rules_flag: Dict[GameName, SpecialRules] = dict()

waiting_players: Dict[GameName, Dict[Player, WebSocket]] = defaultdict(lambda: dict())


async def index(request: Request) -> _TemplateResponse:
    return templates.TemplateResponse(
        "index.html",
        dict(
            request=request,
            special_rules={
                rule.value: special_rules_descriptions[rule] for rule in SpecialRules
            },
        ),
    )


class StateResponse(TypedDict, total=False):
    """
    Describes a response send to the clients.
    """

    game_state: str
    game_state_explanation: str
    cards_of_player: List[Dict[str, Union[str, int]]]
    other_players_cards: Dict[Player, int]
    player_to_take_action: Player
    name_of_player: Player
    order_of_players: List[Player]
    bids_and_scores_table: List[List[str]]
    trump_suit: Optional[Dict[str, str]]
    cards_of_trick: List[Dict[str, Union[str, int]]]


class MessageType(Enum):
    "Types of the messages we send"
    BadInput = "The input you provided was invalid"
    Welcome = "Welcome to the Wizard game server of the drunken beasts"
    StateUpdate = "The state of the game changed"


async def send_message(
    socket: WebSocket,
    msg_type: MessageType,
    msg: str,
    state: Optional[StateResponse] = None,
) -> None:
    msg_dict: Dict[str, Union[str, StateResponse]] = dict(
        msg_type=msg_type.name, msg=f"{msg_type.value}: {msg}"
    )
    if state:
        msg_dict["state"] = state
    await socket.send_json(msg_dict)


async def update_client_state(game: Game) -> None:
    """
    Transmits the state of the game to every client.
    """
    current_cards_of_players = {
        player: all_cards[-1]
        for player, all_cards in game.round.cards_of_players.items()
    }
    state_response = StateResponse(
        game_state=game.state.name,
        game_state_explanation=game.state.value,
        player_to_take_action=game.player_to_take_action,
        order_of_players=list(game.players),
        bids_and_scores_table=bids_and_scores_table(game.players, game,),
        cards_of_trick=[
            card_to_json(card, player)
            for card, player in game.round.cards_of_current_trick.items()
        ],
        trump_suit=None
        if game.round.trump_suit is None
        else dict(color=game.round.trump_suit.value, suit=game.round.trump_suit.name),
    )
    for player in game.players:
        state_response.update(
            dict(
                name_of_player=player,
                cards_of_player=[
                    card_to_json(card) for card in current_cards_of_players[player]
                ],
                other_players_cards={
                    other_player: len(current_cards_of_players[other_player])
                    for other_player in game.players
                    if other_player != player
                },
            ),
        )
        try:
            await send_message(
                player_to_socket[game.name][player],
                MessageType.StateUpdate,
                game.state.value,
                state_response,
            )
        except KeyError:
            print(f"Could not reach player {player} of game {game.name}")
            continue


async def game_websocket(websocket: WebSocket) -> None:
    await websocket.accept()
    game_data = await websocket.receive_json()
    if not game_data["name_of_game"]:
        await send_message(
            websocket, MessageType.BadInput, "No name of any game was given"
        )
        await websocket.close()
        return
    game_name = GameName(game_data["name_of_game"])
    if len(game_data.get("name_of_player", "")) < 3:
        await send_message(
            websocket,
            MessageType.BadInput,
            "Please select a name with at least 4 characters.",
        )
        await websocket.close()
        return
    player = Player(game_data["name_of_player"])
    # trying to join an existing game
    if game_name in games:
        # maybe the player lost the connection and wants to rejoin?
        disconnected_players = set(games[game_name].players) - set(
            player_to_socket[game_name]
        )
        if player in disconnected_players:
            player_to_socket[game_name][player] = websocket
            await update_client_state(games[game_name])
        else:
            await send_message(
                websocket,
                MessageType.BadInput,
                "The game you tried to join is already running",
            )
            await websocket.close()
            return
    # waiting for the others players of the requested game
    elif game_name in waiting_players:
        if player in waiting_players[game_name]:
            await send_message(
                websocket,
                MessageType.BadInput,
                "This name is already taken inside the game.",
            )
            await websocket.close()
            return
        waiting_players[game_name][player] = websocket
    # request to wait for other players of a new game
    else:
        try:
            number_of_players[game_name] = game_data["number_of_players"]
            special_rules_flag[game_name] = SpecialRules(
                game_data.get("special_rules_flag", 0)
            )
        except (IndexError, ValueError) as e:
            await send_message(websocket, MessageType.BadInput, f"Error {e}")
            await websocket.close()
            return
        waiting_players[game_name][player] = websocket
    await send_message(
        websocket,
        MessageType.Welcome,
        f"We are currently {len(waiting_players[game_name])} of "
        f"{number_of_players[game_name]} players. The name of the game is `{game_name}`.",
    )
    if len(waiting_players[game_name]) == number_of_players[game_name]:
        games[game_name] = start_game(
            game_name, list(waiting_players[game_name]), special_rules_flag[game_name]
        )
        transfer_waiting_players_to_game(games[game_name])
        await update_client_state(games[game_name])
    try:
        while True:
            msg = await websocket.receive_text()
            if games[game_name].player_to_take_action != player:
                await send_message(
                    websocket,
                    MessageType.BadInput,
                    "Currently not waiting for any action on your part",
                )
            else:
                try:
                    games[game_name] = await game_action(msg, games[game_name])
                    await update_client_state(games[game_name])
                except ValueError as e:
                    print(e)
                    await send_message(websocket, MessageType.BadInput, str(e))
    finally:
        if game_name in waiting_players and player in waiting_players[game_name]:
            del waiting_players[game_name][player]
        if game_name in games:
            del player_to_socket[game_name][player]
        # delete a game if all players disconnected
        if not any(
            player in player_to_socket[game_name] for player in games[game_name].players
        ):
            del games[game_name]
            del player_to_socket[game_name]


async def game_action(msg: str, game: Game) -> Game:
    """
    This method assumes that the player who passed the msg is allowed to make a move.
    """
    if game.state is GameState.WAITING_FOR_TRUMP_WISH:
        game = pass_trump_wish(game, suit=Suit(msg))
    elif game.state is GameState.WAITING_FOR_BIDS:
        game = pass_bid(game, bid=int(msg))
    elif game.state is GameState.WAITING_FOR_CARDS:
        color, value = msg.split()
        rank: Union[str, int]
        try:
            rank = int(value)
        except ValueError:
            rank = value
        game = play_card(
            game, card=Card(suit=Suit(color.capitalize()), rank=Rank(rank))
        )
        # wait a few seconds prior to cleaning the trick to allow players to take a look
        # at the last card
        state_to_function = {
            GameState.TRICK_FINISHED: start_new_trick,
            GameState.ROUND_FINISHED: update_scores,
        }
        if game.state in state_to_function:
            await update_client_state(game)
            await asyncio.sleep(5)
            game = state_to_function[game.state](game)
    return game


def transfer_waiting_players_to_game(game: Game) -> None:
    for waiting_player in waiting_players[game.name]:
        player_to_socket[game.name][waiting_player] = waiting_players[game.name][
            waiting_player
        ]
    del waiting_players[game.name]


def create_app() -> Starlette:
    routes = [
        Route("/", endpoint=index),
        WebSocketRoute("/ws", endpoint=game_websocket),
        Mount(path="/static", app=StaticFiles(directory="static"), name="static"),
    ]

    app = Starlette(routes=routes)

    return app
