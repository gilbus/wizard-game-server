class StateError(Exception):
    "Raised if the state of the game does not allow the call of the function"
    pass
