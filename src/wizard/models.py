from __future__ import annotations

from collections import defaultdict
from dataclasses import dataclass
from dataclasses import field
from dataclasses import replace
from enum import Enum
from enum import Flag
from enum import auto
from enum import unique
from typing import Any
from typing import Dict
from typing import List
from typing import NewType
from typing import Optional
from typing import Tuple
from typing import TypedDict


@unique
class Suit(Enum):
    Zwerge = "Red"
    Elfen = "Green"
    Riesen = "Yellow"
    Menschen = "Blue"


@unique
class Rank(Enum):
    Dieb = 1
    Magd = 2
    Fischer = 3
    Bäuerin = 4
    Schmied = 5
    Händlerin = 6
    Priester = 7
    Kriegerin = 8
    Feldherr = 9
    Schatzmeisterin = 10
    Prinz = 11
    Königin = 12
    Standarte = 13

    Zauberin = "Z"
    Närrin = "N"


@unique
class GameState(Enum):
    NOT_STARTED_YET = "The game has not started yet"
    WAITING_FOR_TRUMP_WISH = (
        "The trump card is a sorceress and the dealer is allowed to select any suit."
    )
    WAITING_FOR_BIDS = "Cards have been dealt and we are waiting for bids."
    WAITING_FOR_CARDS = "Bids have been taken and the first trick has begun."

    TRICK_FINISHED = "The current trick is finished and the next will start shortly."
    ROUND_FINISHED = "Scores will now be updated and new cards dealt shortly."
    GAME_FINISHED = "The game is finished. Thanks for playing!"


class SpecialRules(Flag):
    DISALLOW_MATCHING_NUMBER_OF_BIDS = auto()
    SHOW_CARDS_TO_OPPONENTS_IN_FIRST_ROUND = auto()

    def __contains__(self, item: Any) -> bool:
        """
        Magic method to check whether one flag is `covered` by another one, ie whether
        all the set bits of the `query`-flag (item) are also set inside our/this flag
        (self).
        Usage of `in` seems more idiomatic in this use case since the user is only
        interested in the fact, whether any given Flag is included in another (possible
        combination of multiple) flags.
        """
        if not isinstance(item, SpecialRules):
            raise NotImplementedError
        return item & self == item


special_rules_descriptions = {
    SpecialRules.DISALLOW_MATCHING_NUMBER_OF_BIDS: """\
            The sum of all bids must not match the number of possible tricks""",
    SpecialRules.SHOW_CARDS_TO_OPPONENTS_IN_FIRST_ROUND: """\
            During the first round: Do not show one's own card but the ones of the
            opponents instead (NOT WORKING YET)""",
}


@dataclass(frozen=True)
class Card:
    """
    Represents a single game card.
    """

    suit: Suit
    rank: Rank


Player = NewType("Player", str)

GameName = NewType("GameName", str)

RoundNumber = NewType("RoundNumber", int)

# stores the cards of all players over all tricks
CardsOfPlayers = Dict[Player, List[Tuple[Card, ...]]]

Bids = Dict[Player, int]

Tricks = Dict[Player, int]


@dataclass(frozen=True)
class Round:

    #: The dealer of this round
    dealer: Player

    #: Cards of players, accessed by
    cards_of_players: CardsOfPlayers

    trump_suit: Optional[Suit]

    cards_of_current_trick: Dict[Card, Player] = field(default_factory=lambda: dict())

    #: Bids of the players
    bids: Bids = field(default_factory=lambda: dict())

    #: Earned tricks of the players
    tricks: Tricks = field(default_factory=lambda: defaultdict(lambda: 0))

    #: Internal counter used in various states
    #: Starts at 1 to ease comparisons with amount of players
    counter: int = 1

    def update(self, updates: RoundArgs) -> Round:
        return replace(self, **updates)


@dataclass(frozen=True)
class UninitializedGame:
    #: Keeps track of the current state of the game, which allows external controllers
    #: to query it.
    state: GameState

    #: The players of the current game
    players: Tuple[Player, ...]

    #: Deck of cards
    deck: Tuple[Card, ...]

    special_rules: Optional[SpecialRules]

    name: GameName


@dataclass(frozen=True)
class Game(UninitializedGame):

    #: Player of which an action is required/expected, which one depends on the state
    player_to_take_action: Player

    round: Round

    previous_rounds: Tuple[Round, ...] = tuple()

    #: How many rounds will the game have
    @property
    def number_of_rounds(self) -> int:
        return int(len(self.deck) / len(self.players))

    @property
    def round_number(self) -> int:
        return len(self.previous_rounds) + 1

    def update(self, updates: GameArgs) -> Game:
        return replace(self, **updates)


# since the dataclasses.replace method is currently not checked by mypy we have to do it
# ourselves :/
class GameArgs(TypedDict, total=False):
    state: GameState
    player_to_take_action: Player
    round: Round
    previous_rounds: Tuple[Round, ...]


class RoundArgs(TypedDict, total=False):
    dealer: Player
    cards_of_players: CardsOfPlayers
    trump_suit: Optional[Suit]
    cards_of_current_trick: Dict[Card, Player]
    bids: Bids
    tricks: Tricks
    counter: int
