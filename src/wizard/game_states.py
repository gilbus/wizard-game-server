"""
Currently unused, but it would be really if dependent typing would work, well...
"""
from typing import ClassVar, TypeVar


class GameState:
    description: ClassVar[str]


class NotStartedYet(GameState):
    description = "The game has not started yet"


class WaitingForTrumpWish(GameState):
    description = """
    The trump card is a sorceress and :var:`Game.player_to_wish_trump` is allowed
    to select any suit :ref:`Suit
    `"""


class WaitingForBids(GameState):
    description = "Cards have been dealt"


class WaitingForCards(GameState):
    description = "Bids have been taken"


class TrickFinished(GameState):
    description = "The current trick is finished and the next will start shortly."


class RoundFinished(GameState):
    description = "Scores will now be updated"


GS = TypeVar("GS", bound=GameState)
