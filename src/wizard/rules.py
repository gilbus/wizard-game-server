from logging import getLogger
from typing import Dict
from typing import Iterable
from typing import List
from typing import Optional
from typing import Tuple

from .models import Bids
from .models import Card
from .models import Player
from .models import Rank
from .models import Suit
from .models import Tricks

_logger = getLogger(__name__)


def create_deck() -> Tuple[Card, ...]:
    return tuple(Card(suit=suit, rank=rank) for suit in Suit for rank in Rank)


def determine_trump(trump_card: Optional[Card]) -> Tuple[Optional[Suit], bool]:
    """
    Determine the trump suit of this round.

    :param trump_card: Card from the remaining deck after the players received their
        cards or None if it is the last round.
    :return: The trump suit of this round and if None whether it was due to sorceress
    """
    if trump_card and trump_card.rank is Rank.Zauberin:
        return None, True
    elif not trump_card or trump_card.rank is Rank.Närrin:
        trump = None
    else:
        trump = trump_card.suit
    return trump, False


def winner_of_trick(cards: List[Card], trump: Optional[Suit]) -> Card:
    """
    Given a number of valid cards and trump return the one winning the current trick.

    :param trump: Trump of the current round or None if no trump is set which happens in
        the last round or if a fool card has been chosen as trump
    :raises ValueError: In case of an empty list of cards
    """
    if not cards:
        raise ValueError("At least one card must be passed")
    # this makes sure that in case of a trick full of jesters the first one wins
    current_winner = cards[0]
    for index, card in enumerate(cards):
        _logger.debug("Current winner: %s", current_winner)
        # first magician of the trick wins
        if card.rank is Rank.Zauberin:
            _logger.debug("First sorceress wins: %s", card)
            return card
        if index == 0:
            continue
        # ignore card since it cannot be higher than the current winner
        if card.rank is Rank.Närrin:
            _logger.debug("Ignoring %s since it is a fool", card)
            continue
        # TODO: refactor in separate method
        elif current_winner.rank is Rank.Närrin:
            current_winner = card
        elif card.suit is current_winner.suit:
            current_winner = (
                current_winner if current_winner.rank.value > card.rank.value else card
            )
        elif card.suit is trump and current_winner.suit is not trump:
            current_winner = card
        # explicitly listing all possible cases for thoroughness
        elif card.suit is not trump and current_winner.suit is trump:
            pass

    return current_winner


def determine_leading_suit(cards: List[Card]) -> Optional[Suit]:
    for card in cards:
        if card.rank is Rank.Närrin:
            continue
        if card.rank is Rank.Zauberin:
            return None
        else:
            return card.suit

    # only jesters so far
    return None


def is_valid_card(
    played_card: Card, trick: List[Card], remaining_cards_of_player: Iterable[Card]
) -> bool:
    """
    Checks whether the played card is valid given all cards of the trick so far, i.e.
    whether the player follows suit if he is able to.

    :param played_card: The card to check
    :param trick: Cards played so far during this trick
    :param players_cards: Cards the current player owns, without played card.
    """

    assert (
        played_card not in remaining_cards_of_player
    ), "Played card cannot be part of remaining cards"

    leading_suit = determine_leading_suit(trick)

    # If no leading suit is set so far any card is valid
    # Sorceresses and jesters can also be played any time
    if leading_suit is None or played_card.rank in (Rank.Zauberin, Rank.Närrin):
        return True
    # If the card is following suit everything is fine
    if played_card.suit is leading_suit:
        return True
    # let's check whether there any cards on the player's hand of the leading suit which
    # he has to follow if possible
    # Check all cards except sorceresses and jesters which are exempt from this rule
    elif not any(
        card.suit is leading_suit
        for card in filter(
            lambda card: card.rank not in (Rank.Zauberin, Rank.Närrin),
            remaining_cards_of_player,
        )
    ):
        return True

    return False


def calculate_scores(bids: Bids, tricks: Tricks,) -> Dict[Player, int]:
    base_for_correct_bid = 20
    points_per_correct_bid = 10
    points_per_bad_bid = -10
    scores = dict()
    for player, number_of_bids in bids.items():
        number_of_tricks = tricks[player]
        if number_of_bids == number_of_tricks:
            score = base_for_correct_bid + number_of_bids * points_per_correct_bid
        else:
            score = abs(number_of_bids - number_of_tricks) * points_per_bad_bid
        scores[player] = score

    return scores
