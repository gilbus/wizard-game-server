from typing import List

from .helper import deal_cards
from .helper import get_next_player
from .helper import requires_state
from .models import Card
from .models import Game
from .models import GameName
from .models import GameState
from .models import Player
from .models import Round
from .models import SpecialRules
from .models import Suit
from .models import UninitializedGame
from .rules import create_deck
from .rules import determine_trump
from .rules import is_valid_card
from .rules import winner_of_trick


def start_game(
    game_name: GameName,
    names_of_players: List[str],
    special_rules: SpecialRules = SpecialRules.DISALLOW_MATCHING_NUMBER_OF_BIDS,
) -> Game:
    if len(names_of_players) != len(set(names_of_players)):
        raise ValueError("Names of players must be unique")
    if not 2 < len(names_of_players) < 7:
        raise ValueError("Game can only be played by 3 to 6 players")
    players = tuple((Player(name) for name in names_of_players))
    deck = create_deck()
    uninitialized_game = UninitializedGame(
        name=game_name,
        state=GameState.NOT_STARTED_YET,
        players=players,
        deck=deck,
        special_rules=special_rules,
    )
    return start_first_round(uninitialized_game)


@requires_state(GameState.NOT_STARTED_YET)
def start_first_round(game: UninitializedGame) -> Game:
    cards_of_players, shuffled_remaining_cards = deal_cards(game.players, game.deck, 1)
    trump_card = shuffled_remaining_cards.pop()
    trump_suit, trump_card_was_sorceress = determine_trump(trump_card)
    if trump_card_was_sorceress:
        player_to_take_action = game.players[-1]
        state = GameState.WAITING_FOR_TRUMP_WISH
        trump_suit = None
    else:
        player_to_take_action = game.players[0]
        state = GameState.WAITING_FOR_BIDS
        trump_suit = trump_card.suit
    return Game(
        name=game.name,
        players=game.players,
        deck=game.deck,
        special_rules=game.special_rules,
        state=state,
        player_to_take_action=player_to_take_action,
        round=Round(
            dealer=game.players[-1],
            trump_suit=trump_suit,
            cards_of_players={
                player: [initial_cards]
                for player, initial_cards in cards_of_players.items()
            },
        ),
    )


@requires_state(GameState.ROUND_FINISHED)
def start_next_round(game: Game) -> Game:
    cards_of_players, shuffled_remaining_cards = deal_cards(
        game.players, game.deck, game.round_number + 1
    )
    trump_card = shuffled_remaining_cards.pop() if shuffled_remaining_cards else None
    new_trump_suit, trump_card_was_sorceress = determine_trump(trump_card)
    new_dealer = get_next_player(game.round.dealer, game.players)
    if trump_card_was_sorceress:
        new_player_to_take_action = new_dealer
        new_state = GameState.WAITING_FOR_TRUMP_WISH
        new_trump_suit = None
    else:
        new_player_to_take_action = get_next_player(new_dealer, game.players)
        new_state = GameState.WAITING_FOR_BIDS
        new_trump_suit = trump_card.suit if trump_card else None

    return game.update(
        dict(
            state=new_state,
            player_to_take_action=new_player_to_take_action,
            previous_rounds=game.previous_rounds + (game.round,),
            # preserve old values
            round=Round(
                dealer=new_dealer,
                trump_suit=new_trump_suit,
                cards_of_players={
                    player: [initial_cards]
                    for player, initial_cards in cards_of_players.items()
                },
            ),
        )
    )


@requires_state(GameState.WAITING_FOR_TRUMP_WISH)
def pass_trump_wish(game: Game, *, suit: Suit) -> Game:
    return game.update(
        dict(
            round=game.round.update(dict(trump_suit=suit)),
            state=GameState.WAITING_FOR_BIDS,
            player_to_take_action=get_next_player(
                game.player_to_take_action, game.players
            ),
        )
    )


@requires_state(GameState.WAITING_FOR_BIDS)
def pass_bid(game: Game, *, bid: int) -> Game:
    if bid < 0:
        raise ValueError("Bids cannot be negative")

    # check whether this is the last bid
    if game.round.counter == len(game.players):
        # check whether the total number of bids is allowed match the possible number of
        # them
        if (
            game.special_rules
            and SpecialRules.DISALLOW_MATCHING_NUMBER_OF_BIDS in game.special_rules
        ):
            if sum(game.round.bids.values()) + bid == game.round_number:
                raise ValueError("Total sum of bids must not match the number of cards")
        new_state = GameState.WAITING_FOR_CARDS
        new_counter = 1
    else:
        new_counter = game.round.counter + 1
        new_state = game.state
    game.round.bids[game.player_to_take_action] = bid
    return game.update(
        dict(
            player_to_take_action=get_next_player(
                game.player_to_take_action, game.players
            ),
            state=new_state,
            # The following is not necessary but better be explicit since we edit it above
            # explicitly
            round=game.round.update(dict(counter=new_counter, bids=game.round.bids),),
        )
    )


@requires_state(GameState.WAITING_FOR_CARDS)
def play_card(game: Game, *, card: Card) -> Game:
    hand_of_player = game.round.cards_of_players[game.player_to_take_action][-1]
    if card not in hand_of_player:
        raise ValueError(
            f"Player {game.player_to_take_action} does not possess card {card}"
        )
    hand_of_player_without_card = tuple(c for c in hand_of_player if c != card)
    if not is_valid_card(
        card, list(game.round.cards_of_current_trick), hand_of_player_without_card,
    ):
        raise ValueError("Invalid card, did you follow suit?")
    game.round.cards_of_players[game.player_to_take_action].append(
        hand_of_player_without_card
    )
    game.round.cards_of_current_trick[card] = game.player_to_take_action

    # Did we get all cards of the current trick?
    if game.round.counter == len(game.players):
        winning_card = winner_of_trick(
            list(game.round.cards_of_current_trick), game.round.trump_suit,
        )
        game.round.tricks[game.round.cards_of_current_trick[winning_card]] += 1
        # did the last player play his last card, i.e. is this round finished?
        if not hand_of_player_without_card:
            return game.update(dict(state=GameState.ROUND_FINISHED))
        else:
            return game.update(dict(state=GameState.TRICK_FINISHED))

    else:
        return game.update(
            dict(
                player_to_take_action=get_next_player(
                    game.player_to_take_action, game.players
                ),
                round=game.round.update(
                    dict(
                        counter=game.round.counter + 1,
                        cards_of_current_trick=game.round.cards_of_current_trick,
                        cards_of_players=game.round.cards_of_players,
                    ),
                ),
            )
        )


@requires_state(GameState.TRICK_FINISHED)
def start_new_trick(game: Game) -> Game:
    """
    The `TRICK_FINISHED` state allows the controller to pause for a short second before
    clearing the cards of the current trick.
    """
    winning_card = winner_of_trick(
        list(game.round.cards_of_current_trick), game.round.trump_suit,
    )
    return game.update(
        dict(
            state=GameState.WAITING_FOR_CARDS,
            # the winner of the last trick plays the first card of the following one
            player_to_take_action=game.round.cards_of_current_trick[winning_card],
            round=game.round.update(dict(counter=1, cards_of_current_trick=dict())),
        )
    )


@requires_state(GameState.ROUND_FINISHED)
def update_scores(game: Game) -> Game:
    # has this been the final round?
    if game.number_of_rounds == game.round_number:
        return game.update(dict(state=GameState.GAME_FINISHED))
    else:
        return start_next_round(game)
