from functools import wraps
from itertools import cycle
from random import sample
from typing import Any
from typing import Callable
from typing import Dict
from typing import Iterable
from typing import List
from typing import Optional
from typing import Tuple
from typing import TypeVar
from typing import Union

from .errors import StateError
from .models import Card
from .models import Game
from .models import GameState
from .models import Player
from .rules import calculate_scores

F = TypeVar("F", bound=Callable[..., Any])


def requires_state(required_state: GameState) -> Callable[..., Callable[..., Game]]:
    def decorator_require_state(func: Callable[..., Game]) -> Callable[..., Game]:
        @wraps(func)
        def inner(game: Game, *args: Any, **kwargs: Any) -> Game:
            if game.state is not required_state:
                raise StateError(
                    f"Wrong state of game. Expected {required_state} got {game.state}"
                )
            return func(game=game, *args, **kwargs)

        return inner

    return decorator_require_state


def deal_cards(
    players: Iterable[Player], deck: Tuple[Card, ...], how_many_cards_per_player: int
) -> Tuple[Dict[Player, Tuple[Card, ...]], List[Card]]:
    shuffled_deck = sample(deck, k=len(deck))
    cards_of_players = {
        player: tuple(shuffled_deck.pop() for _ in range(how_many_cards_per_player))
        for player in players
    }
    return cards_of_players, shuffled_deck


def get_next_player(
    player_whose_successor_is_searched: Player, players: Tuple[Player, ...]
) -> Player:
    assert player_whose_successor_is_searched in players
    players_cycle = cycle(players)
    while next(players_cycle) != player_whose_successor_is_searched:
        continue
    return next(players_cycle)


def card_to_json(
    card: Card, player: Optional[Player] = None
) -> Dict[str, Union[str, int]]:
    return dict(
        suit=card.suit.name,
        color=card.suit.value,
        rank=card.rank.name,
        value=card.rank.value,
        player=player or "",
    )


def bids_and_scores_table(players: Tuple[Player, ...], game: Game,) -> List[List[str]]:
    """
    Recreate the scoring board of the game to send to the client including empty rows.

    |  Player1  |  Player2  | ... |
    |  30 | 1/1 | -10 | 0/1 | ... |
    |  20 | 2/1 |  10 | 0/0 | ... |
    |     |     |     |     |     |
    """
    table: List[List[str]] = [[str(player) for player in players]]
    # previous_scores, previous_bids = bids_and_scores_of_previous_rounds(
    #     players, bids[1:], tricks[1:]
    # )
    intermediate_score: Dict[Player, int] = {}

    for round in game.previous_rounds:
        scores_of_this_round = calculate_scores(round.bids, round.tricks)
        row: List[str] = []
        for player in game.players:
            intermediate_score[player] = scores_of_this_round[
                player
            ] + intermediate_score.get(player, 0)
            row += [
                str(intermediate_score[player]),
                f"{round.tricks[player]}/{round.bids[player]}",
            ]
        table.append(row)
    current_round_bids_and_tricks = []
    for player in game.players:
        # since we've got no score yet
        current_round_bids_and_tricks.append("")
        current_round_bids_and_tricks.append(
            f"{game.round.tricks.get(player,0)}/{game.round.bids.get(player, '?')}"
        )
    table.append(current_round_bids_and_tricks)
    for _ in range(len(game.previous_rounds) + 1, len(game.deck) // len(players)):
        table.append([""] * len(players) * 2)

    return table
