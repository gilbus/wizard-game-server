from typing import List, Tuple

import pytest

from wizard.models import Player


@pytest.fixture
def names_of_players() -> List[str]:
    return ["Player 1", "Player 2", "Player 3", "Player 4"]


@pytest.fixture
def players(names_of_players: List[str]) -> Tuple[Player, ...]:
    return tuple(Player(name) for name in names_of_players)
