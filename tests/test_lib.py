from typing import Any, List

import pytest

from wizard.lib import (determine_trump, pass_bid, pass_trump_wish, play_card,
                        start_game, update_scores)
from wizard.models import Card, GameName, GameState, Rank, SpecialRules, Suit

game_name = GameName("game_name")


@pytest.mark.parametrize(
    ("number_of_players", "allowed"),
    [(1, False), (2, False), (3, True), (4, True), (5, True), (6, True), (7, False)],
)
def test_valid_number_of_players(number_of_players: int, allowed: bool) -> None:
    names = [f"Player {i}" for i in range(number_of_players)]
    if allowed:
        start_game(game_name, names)
    else:
        with pytest.raises(ValueError):
            start_game(game_name, names)
    with pytest.raises(ValueError):
        start_game(game_name, ["a", "b", "c", "c"])


def test_trump_wish(mocker: Any, names_of_players: List[str]) -> None:
    mocked_determine_trump = mocker.patch("wizard.lib.determine_trump")
    mocked_determine_trump.return_value = None, True

    game = start_game(game_name, names_of_players)
    assert game.state is GameState.WAITING_FOR_TRUMP_WISH
    assert game.player_to_take_action is game.players[-1]

    game = pass_trump_wish(game, suit=Suit.Menschen)
    assert game.player_to_take_action is game.players[0]
    assert game.round.trump_suit is Suit.Menschen
    assert game.state is GameState.WAITING_FOR_BIDS


def test_pass_bid(mocker: Any, names_of_players: List[str]) -> None:
    mocked_determine_trump = mocker.patch("wizard.lib.determine_trump")
    mocked_determine_trump.return_value = Suit.Menschen, False
    game = start_game(game_name, names_of_players)

    assert game.state is GameState.WAITING_FOR_BIDS
    assert game.player_to_take_action is game.players[0]

    with pytest.raises(ValueError):
        pass_bid(game, bid=-1)
    game = pass_bid(game, bid=1)
    assert len(game.round.bids) == 1
    assert game.round.bids[game.players[0]] == 1
    assert game.player_to_take_action is game.players[1]
    assert game.state is GameState.WAITING_FOR_BIDS
    for i in range(1, 4):
        assert game.player_to_take_action is game.players[i]
        game = pass_bid(game, bid=1)
    assert len(game.round.bids) == 4


def test_special_rule_matching_forbidden(
    mocker: Any, names_of_players: List[str]
) -> None:
    mocked_determine_trump = mocker.patch("wizard.lib.determine_trump")
    mocked_determine_trump.return_value = Suit.Menschen, False

    game = start_game(
        game_name,
        names_of_players,
        special_rules=SpecialRules.DISALLOW_MATCHING_NUMBER_OF_BIDS,
    )
    assert (
        game.special_rules
        and SpecialRules.DISALLOW_MATCHING_NUMBER_OF_BIDS in game.special_rules
    )
    for _ in range(3):
        game = pass_bid(game, bid=0)
    with pytest.raises(ValueError):
        pass_bid(game, bid=1)
    pass_bid(game, bid=0)


def test_play_card(mocker: Any, names_of_players: List[str]) -> None:
    mocked_determine_trump = mocker.patch("wizard.lib.determine_trump")
    mocked_determine_trump.return_value = Suit.Menschen, False

    game = start_game(game_name, names_of_players)
    for _ in range(4):
        game = pass_bid(game, bid=0)
    assert game.state is GameState.WAITING_FOR_CARDS
    assert game.player_to_take_action is game.players[0]
    with pytest.raises(ValueError):
        # pass something which the player cannot have on its hand
        play_card(game, card=mocker.sentinel)
    game = play_card(game, card=game.round.cards_of_players[game.players[0]][0][0])
    assert game.round.cards_of_players[game.players[0]][1] == ()


def test_invalid_card(mocker: Any, names_of_players: List[str]) -> None:
    mocked_determine_trump = mocker.patch("wizard.lib.determine_trump")
    mocked_determine_trump.return_value = Suit.Menschen, False

    mocked_is_valid_card = mocker.patch("wizard.lib.is_valid_card")
    mocked_is_valid_card.return_value = False

    game = start_game(game_name, names_of_players)
    assert mocked_determine_trump.call_count == 1
    for _ in range(4):
        game = pass_bid(game, bid=0)
    with pytest.raises(ValueError):
        play_card(
            game, card=game.round.cards_of_players[game.players[0]][0][0],
        )
    assert mocked_is_valid_card.call_count == 1


def test_update_scores(mocker: Any, names_of_players: List[str]) -> None:
    mocked_determine_trump = mocker.patch("wizard.lib.determine_trump")
    mocked_determine_trump.return_value = Suit.Menschen, False

    game = start_game(game_name, names_of_players)
    for _ in range(4):
        game = pass_bid(game, bid=0)
    for player in game.players:
        game = play_card(game, card=game.round.cards_of_players[player][0][0])
    assert game.state is GameState.ROUND_FINISHED

    # TODO: move to separate test and fix
    # mocked_determine_trump.return_value = None, True
    # assert game.state is GameState.WAITING_FOR_TRUMP_WISH
