from typing import Any
from typing import List
from typing import Tuple

import pytest

from wizard.errors import StateError
from wizard.helper import card_to_json
from wizard.helper import deal_cards
from wizard.helper import get_next_player
from wizard.helper import requires_state
from wizard.lib import start_game
from wizard.models import Card
from wizard.models import Game
from wizard.models import GameName
from wizard.models import GameState
from wizard.models import Player
from wizard.models import Rank
from wizard.models import RoundNumber
from wizard.models import Suit
from wizard.rules import create_deck

round_number = RoundNumber(1)


def test_card_to_json() -> None:
    assert card_to_json(Card(rank=Rank.Kriegerin, suit=Suit.Zwerge)) == dict(
        suit="Zwerge", color="Red", rank="Kriegerin", value=8, player=""
    )


def test_requires_state(mocker: Any, names_of_players: List[str]) -> None:
    mocked_determine_trump = mocker.patch("wizard.lib.determine_trump")
    mocked_determine_trump.return_value = None, True

    game = start_game(GameName("game_name"), names_of_players)

    @requires_state(GameState.WAITING_FOR_BIDS)
    def foo(game: Game) -> None:
        pass

    @requires_state(GameState.WAITING_FOR_TRUMP_WISH)
    def bar(game: Game) -> None:
        pass

    with pytest.raises(StateError):
        foo(game)
    bar(game)


def test_get_next_player(players: Tuple[Player, ...]) -> None:
    assert get_next_player(players[2], players) == players[3]
    assert get_next_player(players[len(players) - 1], players) == players[0]
