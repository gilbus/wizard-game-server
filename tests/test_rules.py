from random import choice, randint, sample
from typing import List, Optional, cast

import pytest

from wizard.models import Card, Player, Rank, Suit
from wizard.rules import (calculate_scores, create_deck, determine_leading_suit,
                          determine_trump, is_valid_card, winner_of_trick)


def test_create_deck() -> None:
    assert len(create_deck()) == 60, "Wrong length of deck"


test_trick = [
    Card(Suit.Elfen, Rank.Närrin),
    Card(Suit.Menschen, Rank.Standarte),
    Card(Suit.Zwerge, Rank.Dieb),
    Card(Suit.Elfen, Rank.Priester),
    Card(Suit.Riesen, Rank.Fischer),
    Card(Suit.Riesen, Rank.Standarte),
]

fools = [Card(suit, Rank.Närrin) for suit in Suit]


@pytest.mark.parametrize(
    ["cards", "trump", "winner", "constellation"],
    [
        # not relevant for the actual game but to make sure that we always have a winner
        (
            [Card(Suit.Menschen, Rank.Dieb)],
            None,
            Card(Suit.Menschen, Rank.Dieb),
            "One card only",
        ),
        # In case of fools only the first one wins
        (fools, None, fools[0], "Only fools"),
        # random selection of cards and one sorceress which has to win always
        (
            sample(
                [card for card in create_deck() if card.rank is not Rank.Zauberin], 5,
            )
            + [Card(Suit.Menschen, Rank.Zauberin)],
            choice(list(Suit)),
            Card(Suit.Menschen, Rank.Zauberin),
            "Sorceress wins always",
        ),
        (
            test_trick,
            Suit.Menschen,
            Card(Suit.Menschen, Rank.Standarte),
            "Regular run 1",
        ),
        (test_trick, Suit.Riesen, Card(Suit.Riesen, Rank.Standarte), "Regular run 2",),
        (test_trick, Suit.Zwerge, Card(Suit.Zwerge, Rank.Dieb), "Regular run 3",),
        (test_trick, None, Card(Suit.Menschen, Rank.Standarte), "No trump",),
    ],
)
def test_winner_of_trick(
    cards: List[Card], trump: Optional[Suit], winner: Card, constellation: str
) -> None:
    assert (
        winner_of_trick(cards, trump) == winner
    ), f"Wrong winner in constellation: {constellation}"


def test_disallow_no_card() -> None:
    with pytest.raises(ValueError):
        winner_of_trick([], None)


@pytest.mark.parametrize(
    ["cards", "expected_leading_suit"],
    [
        ([Card(Suit.Riesen, Rank.Königin)], Suit.Riesen),
        ([Card(Suit.Menschen, Rank.Zauberin)], None),
        ([Card(Suit.Menschen, Rank.Närrin)], None),
        ([Card(Suit.Menschen, Rank.Zauberin), Card(Suit.Riesen, Rank.Königin)], None),
        (
            [Card(Suit.Menschen, Rank.Närrin), Card(Suit.Riesen, Rank.Königin)],
            Suit.Riesen,
        ),
        (
            [Card(Suit.Riesen, Rank.Königin), Card(Suit.Menschen, Rank.Bäuerin)],
            Suit.Riesen,
        ),
    ],
)
def test_determine_leading_suit(
    cards: List[Card], expected_leading_suit: Optional[Suit]
) -> None:
    assert determine_leading_suit(cards) == expected_leading_suit


@pytest.mark.parametrize(
    ["played_card", "trick", "remaining_cards_of_player", "expected_result", "msg"],
    [
        (choice(list(create_deck())), [], [], True, "First card must always be valid",),
        (
            Card(Suit.Menschen, Rank.Zauberin),
            [],
            [
                card
                for card in sample(create_deck(), randint(1, 10))
                if card != Card(Suit.Menschen, Rank.Zauberin)
            ],
            True,
            "Sorceress must always be valid",
        ),
        # the switch of the empty list between 'trick' and 'remaining_cards_of_player'
        # is on purpose
        (
            Card(Suit.Menschen, Rank.Närrin),
            [
                card
                for card in sample(create_deck(), randint(1, 5))
                if card != Card(Suit.Menschen, Rank.Närrin)
            ],
            [],
            True,
            "Jester must always be valid",
        ),
        (
            Card(Suit.Zwerge, Rank.Kriegerin),
            [Card(Suit.Zwerge, Rank.Bäuerin), Card(Suit.Elfen, Rank.Priester)],
            [Card(Suit.Zwerge, Rank.Priester), Card(Suit.Menschen, Rank.Dieb)],
            True,
            "Following suit must be valid",
        ),
        (
            Card(Suit.Menschen, Rank.Kriegerin),
            [Card(Suit.Zwerge, Rank.Bäuerin), Card(Suit.Elfen, Rank.Priester)],
            [
                Card(Suit.Elfen, Rank.Kriegerin),
                Card(Suit.Menschen, Rank.Dieb),
                Card(Suit.Menschen, Rank.Närrin),
                Card(Suit.Menschen, Rank.Zauberin),
            ],
            True,
            "Not following suit is only valid if not possible",
        ),
        (
            Card(Suit.Menschen, Rank.Kriegerin),
            [Card(Suit.Zwerge, Rank.Bäuerin), Card(Suit.Elfen, Rank.Priester)],
            [
                Card(Suit.Zwerge, Rank.Kriegerin),
                Card(Suit.Menschen, Rank.Dieb),
                Card(Suit.Menschen, Rank.Närrin),
                Card(Suit.Menschen, Rank.Zauberin),
            ],
            False,
            "Not following suit is only valid if not possible",
        ),
    ],
)
def test_is_valid_card(
    played_card: Card,
    trick: List[Card],
    remaining_cards_of_player: List[Card],
    expected_result: bool,
    msg: str,
) -> None:
    assert (
        is_valid_card(played_card, trick, remaining_cards_of_player) == expected_result
    ), msg


def test_determine_trump() -> None:
    assert determine_trump(None) == (
        None,
        False,
    ), "Trump suit selected but no card was passed"
    assert determine_trump(Card(Suit.Menschen, Rank.Zauberin)) == (None, True)
    for suit in list(Suit):
        assert determine_trump(Card(suit, Rank.Magd)) == (suit, False)


@pytest.mark.parametrize(
    ("bids", "tricks", "expected_score"),
    [(1, 1, 30), (1, 2, -10), (2, 1, -10), (0, 0, 20)],
)
def test_calculate_scores(bids: int, tricks: int, expected_score: int) -> None:
    player = Player("P")
    assert calculate_scores({player: bids}, {player: tricks},)[player] == expected_score
