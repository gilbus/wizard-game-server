from typing import Any
from typing import Dict
from typing import Iterable
from typing import List
from typing import Literal
from typing import Optional
from typing import Tuple

import pytest

from wizard.lib import pass_bid
from wizard.lib import pass_trump_wish
from wizard.lib import play_card
from wizard.lib import start_game
from wizard.lib import start_new_trick
from wizard.lib import update_scores
from wizard.models import Card
from wizard.models import GameName
from wizard.models import GameState
from wizard.models import Player
from wizard.models import Rank
from wizard.models import RoundNumber
from wizard.models import SpecialRules
from wizard.models import Suit


def my_deal_cards(
    players: Iterable[Player],
    deck: Tuple[Card, ...],
    how_many_cards_per_player: Literal[1, 2, 3],
) -> Tuple[Dict[Player, Tuple[Card, ...]], List[Card]]:
    hand_cards: Dict[Player, Tuple[Card, ...]]
    trump_card: Optional[Card]
    if how_many_cards_per_player == 1:
        hand_cards = {
            Player("Player 1"): (Card(Suit.Zwerge, Rank.Dieb),),
            Player("Player 2"): (Card(Suit.Zwerge, Rank.Königin),),
            Player("Player 3"): (Card(Suit.Elfen, Rank.Königin),),
        }
        trump_card = Card(Suit.Riesen, Rank.Zauberin)
    elif how_many_cards_per_player == 2:
        hand_cards = {
            Player("Player 1"): (
                Card(Suit.Zwerge, Rank.Dieb),
                Card(Suit.Elfen, Rank.Königin),
            ),
            Player("Player 2"): (
                Card(Suit.Zwerge, Rank.Königin),
                Card(Suit.Riesen, Rank.Zauberin),
            ),
            Player("Player 3"): (
                Card(Suit.Zwerge, Rank.Närrin),
                Card(Suit.Riesen, Rank.Königin),
            ),
        }
        trump_card = Card(Suit.Riesen, Rank.Dieb)
    else:
        hand_cards = {
            Player("Player 1"): (
                Card(Suit.Elfen, Rank.Zauberin),
                Card(Suit.Zwerge, Rank.Zauberin),
                Card(Suit.Zwerge, Rank.Närrin),
            ),
            Player("Player 2"): (
                Card(Suit.Riesen, Rank.Zauberin),
                Card(Suit.Elfen, Rank.Königin),
                Card(Suit.Riesen, Rank.Königin),
            ),
            Player("Player 3"): (
                Card(Suit.Zwerge, Rank.Dieb),
                Card(Suit.Riesen, Rank.Dieb),
                Card(Suit.Zwerge, Rank.Königin),
            ),
        }
        trump_card = None
    return (
        hand_cards,
        [
            card
            for card in deck
            if card
            not in {hand_card for hand in hand_cards.values() for hand_card in hand}
            | ({trump_card} if trump_card else set())
        ]
        + ([trump_card] if trump_card else []),
    )


def test_play_whole_game(mocker: Any, names_of_players: List[str]) -> None:
    names_of_players = names_of_players[:3]
    custom_deck = (
        tuple(
            Card(rank=rank, suit=suit)
            for rank in (Rank.Königin, Rank.Zauberin)
            for suit in (Suit.Zwerge, Suit.Elfen, Suit.Riesen)
        )
        + tuple(Card(rank=Rank.Dieb, suit=suit) for suit in (Suit.Zwerge, Suit.Riesen))
        + (Card(Suit.Zwerge, Rank.Närrin),)
    )
    assert len(custom_deck) == 9

    mocked_create_deck = mocker.patch("wizard.lib.create_deck")
    mocked_create_deck.return_value = custom_deck

    mocked_deal_cards = mocker.patch("wizard.lib.deal_cards")
    mocked_deal_cards.side_effect = my_deal_cards

    ######
    # Start and round 1
    ######

    game = start_game(
        GameName("game_name"),
        names_of_players,
        special_rules=SpecialRules.DISALLOW_MATCHING_NUMBER_OF_BIDS,
    )
    assert game.number_of_rounds == 3
    assert game.name == "game_name"

    mocked_create_deck.assert_called_once_with()
    assert game.state is GameState.WAITING_FOR_TRUMP_WISH

    assert game.player_to_take_action == Player(names_of_players[-1])
    game = pass_trump_wish(game, suit=Suit.Elfen)
    assert game.state is GameState.WAITING_FOR_BIDS
    assert game.round.trump_suit is Suit.Elfen

    assert game.player_to_take_action == Player(names_of_players[0])
    game = pass_bid(game, bid=0)
    assert set(game.round.bids.items()) == {("Player 1", 0)}
    assert game.state is GameState.WAITING_FOR_BIDS

    assert game.player_to_take_action == Player(names_of_players[1])
    game = pass_bid(game, bid=1)
    # let's simulate a typing error
    with pytest.raises(ValueError):
        game = pass_bid(game, bid=-1)
    game = pass_bid(game, bid=1)
    assert set(game.round.bids.items()) == {
        ("Player 1", 0),
        ("Player 2", 1),
        ("Player 3", 1),
    }
    assert game.state is GameState.WAITING_FOR_CARDS

    assert game.player_to_take_action == Player(names_of_players[0])
    # let's try to cheat
    with pytest.raises(ValueError):
        game = play_card(game, card=Card(Suit.Menschen, Rank.Dieb))
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Dieb))
    assert game.round.cards_of_current_trick == {
        Card(Suit.Zwerge, Rank.Dieb): Player("Player 1")
    }

    assert game.player_to_take_action == Player(names_of_players[1])
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Königin))

    game = play_card(game, card=Card(Suit.Elfen, Rank.Königin))
    assert game.state is GameState.ROUND_FINISHED
    assert game.round.cards_of_players[Player("Player 1")] == [
        (Card(Suit.Zwerge, Rank.Dieb),),
        tuple(),
    ]
    assert game.round.cards_of_players[Player("Player 2")] == [
        (Card(Suit.Zwerge, Rank.Königin),),
        tuple(),
    ]
    assert game.round.cards_of_players[Player("Player 3")] == [
        (Card(Suit.Elfen, Rank.Königin),),
        tuple(),
    ]

    game = update_scores(game)

    ######
    # Round 2
    ######

    assert game.state is GameState.WAITING_FOR_BIDS
    assert game.round.trump_suit is Suit.Riesen
    assert game.player_to_take_action == Player("Player 2")

    game = pass_bid(game, bid=1)
    game = pass_bid(game, bid=1)
    assert set(game.round.bids.items()) == {
        ("Player 2", 1),
        ("Player 3", 1),
    }
    # Last player wants to bid 0 but is not allowed to due to special rule #1
    with pytest.raises(ValueError):
        pass_bid(game, bid=0)
    game = pass_bid(game, bid=1)

    assert game.state is GameState.WAITING_FOR_CARDS
    assert game.player_to_take_action == Player("Player 2")

    game = play_card(game, card=Card(Suit.Riesen, Rank.Zauberin))
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Närrin))
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Dieb))
    # Trick 1/2 finished

    assert game.state is GameState.TRICK_FINISHED
    game = start_new_trick(game)
    assert game.state is GameState.WAITING_FOR_CARDS

    # Winner of the previous trick starts the new one
    assert game.player_to_take_action == Player("Player 2")
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Königin))
    game = play_card(game, card=Card(Suit.Riesen, Rank.Königin))
    game = play_card(game, card=Card(Suit.Elfen, Rank.Königin))
    # Trick 2/2 finished

    assert game.state is GameState.ROUND_FINISHED
    game = update_scores(game)

    ######
    # Round 3
    ######

    assert game.player_to_take_action == Player("Player 3")
    game = pass_bid(game, bid=0)
    game = pass_bid(game, bid=2)
    game = pass_bid(game, bid=2)
    assert game.player_to_take_action == Player("Player 3")

    game = play_card(game, card=Card(Suit.Riesen, Rank.Dieb))
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Zauberin))
    # forgets to follow leading suit
    with pytest.raises(ValueError):
        game = play_card(game, card=Card(Suit.Elfen, Rank.Königin))
    game = play_card(game, card=Card(Suit.Riesen, Rank.Königin))

    assert game.state is GameState.TRICK_FINISHED
    game = start_new_trick(game)
    game = play_card(game, card=Card(Suit.Elfen, Rank.Zauberin))
    game = play_card(game, card=Card(Suit.Elfen, Rank.Königin))
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Königin))

    assert game.state is GameState.TRICK_FINISHED
    game = start_new_trick(game)
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Närrin))
    game = play_card(game, card=Card(Suit.Riesen, Rank.Zauberin))
    game = play_card(game, card=Card(Suit.Zwerge, Rank.Dieb))

    assert game.state is GameState.ROUND_FINISHED
    game = update_scores(game)
    assert game.state is GameState.GAME_FINISHED
