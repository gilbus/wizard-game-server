.DEFAULT_GOAL:=help

.PHONY: help
help: ## Display this help message
	@echo 'Usage: make <command>'
	@cat $(MAKEFILE_LIST) | grep '^[a-zA-Z]'  | \
	    sort | \
	    awk -F ':.*?## ' 'NF==2 {printf "  %-26s%s\n", $$1, $$2}'

.PHONY: clean-pyc
clean-pyc: ## Remove python bytecode files and folders such as __pycache__
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '*.pyo' -exec rm --force {} +
	find . -type d -name '__pycache__' -prune -exec rm -rf {} \;
	rm -rf .mypy_cache

.PHONY: clean-build
clean-build: ## Remove any python build artifacts
	rm --force --recursive build/
	rm --force --recursive dist/
	rm --force --recursive *.egg-info

.PHONY: test
test: ## Run test suite
	poetry run pytest --color=yes

.PHONY: mypy
# if tests contain potential errors they cannot test correctly
mypy: ## Run `mypy`, a static type checker for python, see 'htmlcov/mypy/index.html'
	poetry run mypy src/wizard tests --html-report=htmlcov/mypy
