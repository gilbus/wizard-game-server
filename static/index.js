/*jshint esversion: 6 */

// thanks stackoverflow ;)
function $(selector, el) {
  if (!el) {
    el = document;
  }
  return el.querySelector(selector);
}
function $$(selector, el) {
  if (!el) {
    el = document;
  }
  return Array.prototype.slice.call(el.querySelectorAll(selector));
  // Note: the returned object is a NodeList.
  // If you'd like to convert it to a Array for convenience, use this instead:
  // return Array.prototype.slice.call(el.querySelectorAll(selector));
}
function remove_all_child_nodes(elem) {
  while (elem.hasChildNodes()) {
    elem.removeChild(elem.firstChild);
  }
}

var ws;

function state_update(state) {
  update_player_order(state.order_of_players, state.player_to_take_action);
  update_players_cards(state.cards_of_player);
  update_bids_and_scores_table(state.bids_and_scores_table);
  update_cards_of_trick(state.cards_of_trick);
  update_trump_suit(state.trump_suit);
  $("#additional_controls").style.display = "none";
  $("#select_suit").style.display = "none";
  $("#select_number_of_bids").style.display = "none";
  $("#play_card_button").style.display = "none";
  if (state.name_of_player != state.player_to_take_action) {
    return;
  }
  switch (state.game_state) {
    case "WAITING_FOR_TRUMP_WISH":
      $("#additional_controls").style.display = "block";
      $("#select_suit").style.display = "inline-grid";
      break;
    case "WAITING_FOR_BIDS":
      $("#additional_controls").style.display = "block";
      $("#select_number_of_bids").style.display = "block";
      break;
    case "WAITING_FOR_CARDS":
      $("#play_card_button").style.display = "block";
      break;
    default:
  }
}

function add_message(msg) {
  var msg_list = $("#messages");
  if (
    msg_list.childNodes.length > 0 &&
    msg == msg_list.childNodes[0].childNodes[0].data
  ) {
    return;
  }
  while (msg_list.childNodes.length > 4) {
    msg_list.removeChild(msg_list.childNodes[4]);
  }
  msg_elem = document.createElement("li");
  msg_elem.innerText = msg;
  msg_list.prepend(msg_elem);
}

function handle_ws_msg(event) {
  var data = JSON.parse(event.data);
  console.log(data);
  if (data.msg) {
    add_message(data.msg);
  }
  switch (data.msg_type) {
    case "StateUpdate":
      state_update(data.state);
      break;
  }
}

function update_trump_suit(trump_suit) {
  const base_text = "Current cards of trick";
  var trump_suit_color, trump_suit_name;
  if (trump_suit == null) {
    trump_suit_color = "black";
    trump_suit_name = "Keiner";
  } else {
    trump_suit_color = trump_suit.color;
    trump_suit_name = trump_suit.suit;
  }
  $("#trick > legend").firstChild.data =
    base_text + " - " + "Trumpf: " + trump_suit_name;
  $("#trick").style.borderColor = trump_suit_color;
}

function update_cards_of_trick(cards) {
  var trick = $("#trick_cards");
  remove_all_child_nodes(trick);
  cards.forEach((card) => {
    trick.appendChild(create_card(card, false));
  });
}

function update_bids_and_scores_table(gametable) {
  const table = $("#bids_and_scores_table");
  remove_all_child_nodes(table);
  first_row = document.createElement("tr");
  var number_of_round = document.createElement("th");
  number_of_round.innerText = "#";
  first_row.append(number_of_round);
  var head, row;
  gametable[0].forEach((player) => {
    head = document.createElement("th");
    head.setAttribute("colspan", 2);
    head.innerText = player;
    first_row.append(head);
  });
  table.appendChild(first_row);
  for (var i = 1; i < gametable.length; i++) {
    row = document.createElement("tr");
    number_of_round = document.createElement("td");
    number_of_round.innerText = i;
    row.append(number_of_round);
    for (var j = 0; j < gametable[i].length; j++) {
      entry = document.createElement("td");
      entry.innerText = gametable[i][j];
      row.appendChild(entry);
    }
    table.appendChild(row);
  }
}

function submit_trump_wish() {
  ws.send($("input[name=suit]:checked").value);
}

function play_card() {
  ws.send($("input[name=card]:checked").value);
}

function pass_bid() {
  ws.send($("#number_of_bids").value);
}

function update_players_cards(gamecards) {
  var cards = $("#cards");
  remove_all_child_nodes(cards);
  gamecards.forEach((gamecard) => {
    console.log(gamecard, gamecards);
    cards.appendChild(create_card(gamecard, true));
  });
}

function update_player_order(players, player_to_take_action) {
  sidebar = $("#order_of_players");
  remove_all_child_nodes(sidebar);
  for (var i = 0; i < players.length; i++) {
    var down_arrow = document.createElement("div");
    down_arrow.innerText = "⬇️";
    var name = document.createElement("div");
    name.innerText = players[i];
    if (players[i] == player_to_take_action) {
      name.classList.add("active_player");
    }
    sidebar.append(name);
    if (i == players.length - 1) {
      break;
    }
    sidebar.append(down_arrow);
  }
}

function init() {
  $$(".game_element").forEach((elem) => {
    elem.style.display = "none";
  });
}

function connect_ws() {
  const game_data = {
    name_of_player: $("#name_of_player").value,
    number_of_players: parseInt($("#number_of_players").value),
    name_of_game: $("#name_of_game").value,
    special_rules_flag: $$("input[name=special_rule]:checked").reduce(
      (previous, current) => previous + parseInt(current.dataset.flag),
      0
    ),
  };
  ws = new WebSocket(
    `${window.location.protocol == "https" ? "wss:" : "ws:"}//${
      window.location.host
    }/ws`
  );
  console.log(ws);
  ws.onopen = () => ws.send(JSON.stringify(game_data));

  ws.onmessage = handle_ws_msg;

  $("#initial").style.display = "none";
  $$(".game_element").forEach(function (elem) {
    if (elem.id == "additional_controls") {
      return;
    }
    elem.style.display = "";
  });
}

function create_card(gamecard, as_input = false) {
  console.log(gamecard);
  var card = document.createElement("div");
  var card_css = ["card", gamecard.color.toLowerCase()];
  card.classList.add(...card_css);

  var value = document.createElement("h2");
  value.innerText = gamecard.value;

  var details = document.createElement("div");
  details.classList.add("card_details");

  var rank = document.createElement("h4");
  rank.classList.add("rank");
  rank.innerText = gamecard.rank;

  var suit = document.createElement("p");
  suit.innerText = gamecard.suit;

  details.append(rank, suit);

  card.append(value, details);
  if (gamecard.player) {
    var player_name = document.createElement("span");
    player_name.classList.add("player_name");
    value.before(player_name, gamecard.player);
  }
  if (as_input) {
    var label = document.createElement("label");
    var input = document.createElement("input");
    input.setAttribute("type", "radio");
    input.setAttribute("name", "card");
    input.setAttribute("value", `${gamecard.color} ${gamecard.value}`);
    label.appendChild(input);
    label.appendChild(card);
    return label;
  } else {
    return card;
  }
}
